import { CUSTOMERS, FIELDS } from "./../source";
import { CUSTOMERS_LIST, FIELDS_LIST } from "./../constants";
import { getData, storeData } from "./useLocalStorage";

const loadCustomers = () => {
  // Function to load customers
  const customers = getData(CUSTOMERS_LIST);
  if (typeof customers === "object" && customers.length === 0) {
    storeData(CUSTOMERS_LIST, CUSTOMERS);
  }
};

const loadFields = () => {
  // Function to load fields
  const fields = getData(FIELDS_LIST);
  if (typeof fields === "object" && fields.length === 0) {
    storeData(FIELDS_LIST, FIELDS);
  }
};

export const loadResources = () => {
  loadCustomers();
  loadFields();
};
