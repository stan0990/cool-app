import React from "react";
import Field from "./Field";

export class Form extends React.Component {
  state = { devMode: false };

  onChangeDevMode = () => {
    //Switching dev Mode boolean value
    const { devMode } = this.state;
    this.setState({ devMode: !devMode });
  };

  render() {
    const {
      fields,
      onChangeInputValue,
      onChangeChecked,
      form,
      sendForm
    } = this.props;
    const { devMode } = this.state;

    return (
      <>
        <input type="checkbox" onChange={this.onChangeDevMode} />
        <span>Modo developer </span>
        <hr />
        {fields !== undefined ? (
          fields.map((field, index) => (
            <Field
              key={index}
              field={field}
              devVisible={devMode}
              onChange={onChangeInputValue}
              onChangeChecked={onChangeChecked}
              value={form !== undefined ? form[field.name] : undefined}
            />
          ))
        ) : (
          <div></div>
        )}

        <button onClick={sendForm} className="send-button" disabled={devMode}>
          Enviar
        </button>
      </>
    );
  }
}
