import React from "react";
import { inputType } from "./Inputs";

export default ({
  field,
  arrayOptions,
  devVisible,
  onChange,
  onChangeChecked,
  value
}) => {
  return devVisible ? (
    <div className="form-item">
      <label>{field.label}: </label>
      <br />
      {inputType(field, onChange, value, devVisible)}
      {devVisible && (
        <>
          <input
            type="checkbox"
            checked={field.isVisible}
            onChange={() => onChangeChecked(field.name)}
          />
          Visible
        </>
      )}
    </div>
  ) : field.isVisible ? (
    <div className="form-item">
      <label>{field.label}: </label>
      <br />
      {inputType(field, onChange, value)}
    </div>
  ) : (
    <div></div>
  );
};
