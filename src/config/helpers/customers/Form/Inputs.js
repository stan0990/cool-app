import React from "react";

// Helper to render inputs depending de prop defined

export const inputType = (field, onChange, value, devVisible) => {
  switch (field.type) {
    case "string":
      return (
        <input
          name={field.name}
          onChange={onChange}
          value={value}
          readOnly={devVisible}
          className={devVisible ? "disabled-input" : ""}
        />
      );

    case "text":
      return (
        <textarea
          name={field.name}
          readOnly={devVisible}
          onChange={onChange}
          className={devVisible ? "disabled-input" : ""}
          defaultValue={value}
        />
      );

    case "select":
      return (
        <select
          name={field.name}
          onChange={onChange}
          className={devVisible ? "disabled-input" : ""}
          value={value}
        >
          {field.array !== undefined &&
            field.array.length > 0 &&
            field.array.map(item => (
              <option value={item} key={item}>
                {item}
              </option>
            ))}
        </select>
      );

    default:
      return <span></span>;
  }
};
