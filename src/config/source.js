export const CUSTOMERS = [
  {
    id: Math.random(),
    name: "Stanley",
    lastname: "Santacruz",
    phone: "74545920",
    extract: "Developer",
    email: "zstan012@gmail.com",
    birthday: "09-01-1990",
    gender: "Masculino",
    is_deleted: false
  },
  {
    id: Math.random(),
    name: "Isabella Lorena",
    lastname: "Ramirez",
    phone: "64520788",
    extract: "Marketing",
    email: "ilorena@gmailcom",
    birthday: "03-05-1985",
    gender: "Femenino",
    is_deleted: false
  },
  {
    id: Math.random(),
    name: "Adrian Jose",
    lastname: "Vasquez",
    phone: "70558035",
    extract: "Industry",
    email: "avasquez@gmail.com",
    birthday: "06-08-1996",
    gender: "Masculino",
    is_deleted: false
  },
  {
    id: Math.random(),
    name: "Oscar Ricardo",
    lastname: "Sosa Alegria",
    phone: "75800041",
    extract: "QA",
    email: "qaoscar@gmail.com",
    birthday: "09-12-1975",
    gender: "Masculino",
    is_deleted: false
  }
];

export const FIELDS = [
  { name: "name", isVisible: true, label: "Nombre", type: "string" },
  { name: "lastname", isVisible: true, label: "Apellido", type: "string" },
  { name: "phone", isVisible: true, label: "Teléfono", type: "string" },
  { name: "extract", isVisible: true, label: "Resumen", type: "text" },
  { name: "email", isVisible: true, label: "Correo", type: "string" },
  {
    name: "gender",
    isVisible: true,
    label: "Género",
    type: "select",
    array: ["Masculino", "Femenino"]
  }
];
