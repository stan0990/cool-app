import React, { Component } from "react";
import { Link } from "react-router-dom";

import { getData, storeData } from "../../config/helpers/useLocalStorage";
import { CUSTOMERS_LIST } from "../../config/constants";
import ListCustomers from "./ListCustomers";

export class Customers extends Component {
  state = {
    customers: []
  };

  componentDidMount() {
    this.fetchCustomers();
  }

  fetchCustomers = () => {
    // Function for fetching customers from localStorage
    const customers = getData(CUSTOMERS_LIST).filter(
      data => data.is_deleted === false
    );
    this.setState({ customers });
    /* This information will be loaded from API, *customers* will be *data* from backend */
  };

  onArchive = id => {
    /* Function to archive users wich it will change to *true* 
    the is_deleted value, after that we will filter all customers
     with is_deleted = false to show in screen */

    const { customers } = this.state;
    const newCustomers = customers.map(customer => {
      if (customer.id === id) {
        customer = {
          ...customer,
          is_deleted: true
        };
      }
      return customer;
    });
    storeData(CUSTOMERS_LIST, newCustomers);
    window.location.href = "/";
  };

  render() {
    const { customers } = this.state;
    return (
      <div className="AppWrapper">
        <h3>Listado de clientes</h3>
        <Link to="/form">Agregar</Link>
        <ListCustomers customers={customers} onArchive={this.onArchive} />
      </div>
    );
  }
}
