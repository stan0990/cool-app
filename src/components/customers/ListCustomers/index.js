import React from "react";
import CustomerItem from "./CustomerItem";

export default ({ customers, onArchive }) => {
  return (
    <table className="default-table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre Completo</th>
          <th>Phone</th>
          <th>Extracto</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {customers.map((customer, index) => (
          <CustomerItem customer={customer} key={index} onArchive={onArchive} />
        ))}
      </tbody>
    </table>
  );
};
