import React from "react";

export default ({ customer, onArchive }) => (
  <tr>
    <td>{customer.id}</td>
    <td>
      {customer.name} {customer.lastname}
    </td>
    <td>{customer.phone}</td>
    <td>{customer.extract}</td>
    <td>
      <a href={`/form/${customer.id}`}>Editar</a>
      <a onClick={() => onArchive(customer.id)} href="#!">
        Archivar
      </a>
    </td>
  </tr>
);
