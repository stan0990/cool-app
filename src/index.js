import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";

// Importing all containers to configure react router
import { Customers } from "./components/customers/container";
import { CreateCustomer } from "./components/customers/Create/container";
import { EditCustomer } from "./components/customers/Edit/container";
// <<<<<<<<<<<<<<<<<<<<<<<

import { loadResources } from "./config/helpers/loadData";
// All main resources will be loaded in this function.

loadResources(); // Function executed.

ReactDOM.render(
  <Router>
    <Switch>
      <Route exact path="/">
        <Customers />
      </Route>
      <Route exact path="/form">
        <CreateCustomer />
      </Route>
      <Route exact path="/form/:id">
        <EditCustomer />
      </Route>
    </Switch>
  </Router>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
